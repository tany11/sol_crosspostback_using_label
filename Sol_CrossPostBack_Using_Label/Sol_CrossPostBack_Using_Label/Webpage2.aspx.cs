﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CrossPostBack_Using_Label
{
    public partial class Webpage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.PreviousPage != null)
            {
                // Bad Practise.

                //// find control value using Find control and pass parameter as control id
                //TextBox txtName1Obj = PreviousPage.FindControl("txtName1") as TextBox;
                //TextBox txtName2Obj = PreviousPage.FindControl("txtName2") as TextBox;
                //TextBox txtName3Obj = PreviousPage.FindControl("txtName3") as TextBox;
                //TextBox txtName4Obj = PreviousPage.FindControl("txtName4") as TextBox;
                //TextBox txtName5Obj = PreviousPage.FindControl("txtName5") as TextBox;

                //// bind Text Box Value into Lable Control.
                //lblName1.Text = txtName1Obj.Text;
                //lblName2.Text = txtName2Obj.Text;
                //lblName3.Text = txtName3Obj.Text;
                //lblName4.Text = txtName4Obj.Text;
                //lblName5.Text = txtName5Obj.Text;

                // Good Practice.

                // find control value using Find control and pass parameter as control id
                string strName1 =
                    ((TextBox)PreviousPage.FindControl("txtName1")).Text;
                string strName2 =
                    ((TextBox)PreviousPage.FindControl("txtName2")).Text;
                string strName3 =
                    ((TextBox)PreviousPage.FindControl("txtName3")).Text;
                string strName4 =
                    ((TextBox)PreviousPage.FindControl("txtName4")).Text;
                string strName5 =
                    ((TextBox)PreviousPage.FindControl("txtName5")).Text;
                // bind TextBox Value into Lable Control.
                lblName1.Text = strName1;
                lblName2.Text = strName2;
                lblName3.Text = strName3;
                lblName4.Text = strName4;
                lblName5.Text = strName5;

            }
        }
    }
}