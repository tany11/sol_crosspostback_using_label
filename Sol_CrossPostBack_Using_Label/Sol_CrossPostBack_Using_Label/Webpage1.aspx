﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webpage1.aspx.cs" Inherits="Sol_CrossPostBack_Using_Label.Webpage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:TextBox ID="txtName1" runat="server"></asp:TextBox><br>
             <asp:TextBox ID="txtName2" runat="server"></asp:TextBox><br>
             <asp:TextBox ID="txtName3" runat="server"></asp:TextBox><br>
             <asp:TextBox ID="txtName4" runat="server"></asp:TextBox><br>
             <asp:TextBox ID="txtName5" runat="server"></asp:TextBox><br>
             
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" PostBackUrl="~/Webpage2.aspx" />
        </div>
    </form>
</body>
</html>
